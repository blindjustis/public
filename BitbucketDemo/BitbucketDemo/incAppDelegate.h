//
//  incAppDelegate.h
//  BitbucketDemo
//
//  Created by David Hewitt on 06/05/2012.
//  Copyright (c) 2012 Incremental. All rights reserved.
//

#import <UIKit/UIKit.h>

@class incViewController;

@interface incAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) incViewController *viewController;

@end
