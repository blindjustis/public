//
//  Shader.fsh
//  BitbucketDemo
//
//  Created by David Hewitt on 06/05/2012.
//  Copyright (c) 2012 Incremental. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
