//
//  main.m
//  BitbucketDemo
//
//  Created by David Hewitt on 06/05/2012.
//  Copyright (c) 2012 Incremental. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "incAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([incAppDelegate class]));
    }
}
